import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

export class RouterPaths {
  static readonly HOME = '**';
  static readonly MAP = 'map/:id';
}

const ROUTES = [
  {path: RouterPaths.MAP, component: HomeComponent},
  {path: RouterPaths.HOME, component: HomeComponent},
];

const appRoutes: Routes = ROUTES;

@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}