import { NgModule } from '@angular/core';
import { MarkerComponent } from './components/marker.component';
import { PolylineComponent } from './components/polyline.component';
import { PolygonComponent } from './components/polygon.component';
import { MapComponent } from './components/map.component';
import { LeafletCore } from './services/leaflet-core';
import { BROWSER_GLOBALS_PROVIDERS } from './utils/browser-globals';


@NgModule({
  imports: [],
  declarations: [
    PolylineComponent,
    MapComponent,
    MarkerComponent,
    PolygonComponent
  ],
  exports: [
    PolylineComponent,
    MapComponent,
    MarkerComponent,
    PolygonComponent
  ],
  providers: [LeafletCore, BROWSER_GLOBALS_PROVIDERS]
})

export class LeafletModule {
}
