import { Component, Inject, forwardRef, OnInit, Input } from '@angular/core';
import { MapComponent } from './map.component';

@Component({
  selector: 'leaflet-polygon',
  template: ''
})
export class PolygonComponent implements OnInit{

  @Input() latlngs: Array<Array<number>>;

  constructor(@Inject(forwardRef(() => MapComponent)) private _parent:MapComponent) {}

  ngOnInit() {
    this._parent.createPolygon(this.latlngs);
  }

}