import { Component, OnInit, ViewChild, AfterViewInit, Input } from '@angular/core';
import { LeafletCore } from './../services/leaflet-core';

declare const L: any;

@Component({
  selector: 'leaflet-map',
  template: `
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />
    <div #current class="{{styleClass}}"></div>
  `
})
export class MapComponent implements OnInit, AfterViewInit {

  @ViewChild('current') current;
  @Input() coordinates: Array<number>;
  @Input() fitBounds: Array<Array<number>>;
  @Input() maxZoom: number = 21;
  @Input() minZoom: number = 12;
  @Input() tileLayer: string = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  @Input() imageOverlay: string;
  @Input() imageBounds: Array<Array<number>>;
  @Input() styleClass: string;

  private map: any;

  constructor(private leaflet: LeafletCore) {}

  ngOnInit() {
    this.leaflet.init().then(() => {
      this.createMap(this.current.nativeElement);
    }); 
  }

  ngAfterViewInit() {

  }

  createMap(current) {
    this.map = L.map(current);
      L.tileLayer(this.tileLayer).addTo(this.map);

      this.map.options.minZoom = this.minZoom;
      this.map.options.maxZoom = this.maxZoom;

      if (this.fitBounds) {
        this.map.fitBounds(this.fitBounds);
      }

      if (this.imageOverlay && this.imageBounds) {
        L.imageOverlay(this.imageOverlay, this.imageBounds).addTo(this.map);
      }
  }

  createMarker(coordinates: Array<number>, popInfo?:string) {
    this.leaflet.init().then(() => {
      if (this.map) {
        let marker = L.marker(coordinates).addTo(this.map);

        if (popInfo) {
          marker.bindPopup(popInfo).openPopup();
        }
      }
    });
  }

  createPolyne(latlngs: Array<Array<number>>, color?: string, fitBound?: boolean) {

    this.leaflet.init().then(() => {
      if (this.map) {
        if(!color) {
          color = 'red';
        }

        let polyline = L.polyline(latlngs, {color: color}).addTo(this.map);
        
        if (fitBound) {
          this.map.fitBounds(polyline.getBounds());
        }
      }
    });
  }

  createPolygon(latlngs: Array<Array<number>>) {
    this.leaflet.init().then(() => {
      if (this.map) {
        let polygon = L.polygon(latlngs).addTo(this.map);
      }
    });
  }

}