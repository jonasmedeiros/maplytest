import { Component, Inject, forwardRef, OnInit, Input } from '@angular/core';
import { MapComponent } from './map.component';

@Component({
  selector: 'leaflet-polyline',
  template: ''
})
export class PolylineComponent implements OnInit{

  @Input() latlngs: Array<Array<number>>;
  @Input() color: string;
  @Input() fitBound:boolean = false;

  constructor(@Inject(forwardRef(() => MapComponent)) private _parent:MapComponent) {}

  ngOnInit() {
    this._parent.createPolyne(this.latlngs, this.color, this.fitBound);
  }

}