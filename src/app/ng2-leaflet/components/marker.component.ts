import { Component, Inject, forwardRef, OnInit, Input } from '@angular/core';
import { MapComponent } from './map.component';

@Component({
  selector: 'leaflet-marker',
  template: ''
})
export class MarkerComponent implements OnInit{

  @Input() coordinates: Array<number>;
  @Input() popupInfo: string;

  constructor(@Inject(forwardRef(() => MapComponent)) private _parent:MapComponent) {}

  ngOnInit() {
    this._parent.createMarker(this.coordinates, this.popupInfo);
  }

}