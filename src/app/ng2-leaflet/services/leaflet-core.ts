import { Injectable } from '@angular/core';
import { DocumentRef, WindowRef } from './../utils/browser-globals';

@Injectable()
export class LeafletCore {

  private loadLeaflet: Promise<any>;
  private url: string = 'https://unpkg.com/leaflet@1.0.3/dist/leaflet.js';

  constructor(private _document: DocumentRef, private _window: WindowRef) {
    
    this.loadLeaflet = new Promise((resolve, reject) => {
      this.loadMap(resolve, reject);
    });
  }

  public init(): Promise<any> {
    return this.loadLeaflet;
  }

  public loadMap(resolve, reject): Promise<any> {
    const script = this._document.getNativeDocument().createElement('script');
    script.type = 'text/javascript';
    script.src = this.url;

    // if map is already there
    if (this._document.getNativeDocument().body.contains(script)) {
      return;
    }

    if (script.readyState) {  //IE
        script.onreadystatechange = () => {
            if (script.readyState === "loaded" || script.readyState === "complete") {
                script.onreadystatechange = null;
                return resolve({script: name, loaded: true, status: 'Loaded'});
            }
        };
    } else {  //Others
        script.onload = () => {
            return resolve({script: name, loaded: true, status: 'Loaded'});
        };
    }
    
    this._document.getNativeDocument().getElementsByTagName('head')[0].appendChild(script);
  }

}