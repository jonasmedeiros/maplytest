import { Component, OnInit, OnDestroy } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private maps: Array<any>;
  private sub: any;
  private currentMap;
  private id: number;

  private marker: Array<any>;
  private polyline: Array<any>;
  private polygon: Array<any>;
  private bounds: Array<any>;
  private image:string;
  private maxZoom: number;
  private minZoom: number;

  private mapTile: string = 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}';

  constructor(private http: Http, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    
    this.sub = this.route.params.subscribe(param => {

      this.reset();

      this.id = 1;

      if (param['id']) {
        this.id = Number(param['id']);
      }

      this.http.get('assets/map.json').subscribe(
        res => {

          this.maps = res.json();

          if (this.id) {
            this.currentMap = this.maps.find(item => item.id === this.id);
          } else {
            this.currentMap  = this.maps[0];
          }

          if (this.currentMap) {
            this.marker = this.currentMap.annotations.find(item => item.type === 'marker');
            this.polyline = this.currentMap.annotations.find(item => item.type === 'polyline');
            this.polygon = this.currentMap.annotations.find(item => item.type === 'polygon');
            this.bounds = this.currentMap.bounds;
            this.image = this.currentMap.url;
            this.maxZoom = this.currentMap.nativeZoom.max;
            this.minZoom = this.currentMap.nativeZoom.min;
          }

        },
        error =>  console.log(error)
        );

    });
  }

  reset() {
    this.marker = null;
    this.polyline = null;
    this.polygon = null;
    this.bounds = null;
    this.image = null;
    this.maxZoom = null;
    this.minZoom = null;

    this.mapTile = 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}';
  }

  ngOnDestroy() {
    this.sub.unsubscribe();

    this.reset();
  }

  gotTo(id: number) {
    this.router.navigate(['/map', id]);
  }

}
