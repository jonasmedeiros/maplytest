import { MaplyTestPage } from './app.po';

describe('maply-test App', () => {
  let page: MaplyTestPage;

  beforeEach(() => {
    page = new MaplyTestPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
